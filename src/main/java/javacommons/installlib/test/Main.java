package javacommons.installlib.test;

import javacommons.installlib.InstallLibrary;

public class Main {
    public static void main(String[] args0) {
        InstallLibrary il = new InstallLibrary();
        System.out.println(il.getEnv("path"));
        il.setEnv("PATH3", "<path3-xyz>");
        System.out.println(il.getEnv("path3"));
        //il.appendToPath("PATH", "x");
        il.prependToPath("PATH", "x");
    }
}
