package javacommons.installlib;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import java.util.ArrayList;
import java.util.List;

public class InstallLibrary implements AutoCloseable {

    ActiveXComponent wshell;
    Dispatch userEnv;

    public InstallLibrary() {
        ComThread.InitSTA();
        this.wshell = new ActiveXComponent("WScript.Shell");
        Variant[] args1 = new Variant[]{new Variant("USER")};
        this.userEnv = wshell.invoke("Environment", args1).toDispatch();
    }

    @Override
    public void close() throws Exception {
        ComThread.Release();
    }
    
    public String getEnv(String name) {
        return Dispatch.call(this.userEnv, "Item", name).toString();
    }

    public void setEnv(String name, String value) {
        Dispatch.invoke(this.userEnv, "Item", Dispatch.Put, new Object[]{name, value}, new int[2]);
    }

    public void appendToPath(String name, String x) {
        String path = this.getEnv(name);
        final String[] split = path.split(";");
        List<String> result = new ArrayList<>();
        for(int i=0; i<split.length; i++) {
            String y = split[i];
            if(!x.equalsIgnoreCase(y)) {
                result.add(y);
            }
        }
        result.add(x);
        this.setEnv(name, String.join(";", result));
    }

    public void prependToPath(String name, String x) {
        String path = this.getEnv(name);
        final String[] split = path.split(";");
        List<String> result = new ArrayList<>();
        result.add(x);
        for(int i=0; i<split.length; i++) {
            String y = split[i];
            if(!x.equalsIgnoreCase(y)) {
                result.add(y);
            }
        }
        this.setEnv(name, String.join(";", result));
    }

}
